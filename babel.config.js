module.exports = {
  presets: [
    '@vue/app',
    [
      '@babel/preset-env',
      {
        modules: false,
        targets: {
          browsers: ['> 2%']
        },
        forceAllTransforms: true
      }
    ]
  ],
  plugins: [
    '@babel/plugin-proposal-object-rest-spread',
    [
      '@babel/plugin-transform-runtime',
      {
        helpers: false
      }
    ],
    [
      'import',
      {
        libraryName: 'iview',
        libraryDirectory: 'src/components',
      },
    ],
  ],
}
