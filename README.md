# taxi-booking-widget

## Project setup

```
yarn
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

Widget component:
```
yarn build
```

Widget main loader:
```
yarn build:loader
```

### Test widget integration locally

Compiles and uploads to current '/public' directory
```
sh widget.sh
```

You can pass directory where taxi-app is located as 1st argument
```
sh widget.sh /projects/taxi-app
```

### Lints and fixes files

```
yarn lint
```

See [Configuration Reference](https://cli.vuejs.org/config/).
