export default {
  methods: {
    validate() {
      return new Promise((resolve, reject) => {
        // When validating, iView creates a new promise
        // which never resolves if there is no fields available
        // To prevent this we will just resolve our promise here
        if (this.$refs.mainForm.fields.length === 0) {
          resolve()
          return
        }

        this.$refs.mainForm.validate(valid => {
          if (valid) {
            const { mapFormDataToStore: mapper } = this
            if (mapper && typeof mapper === 'function') {
              mapper.call(this)
            }
            resolve()
          } else {
            reject(new Error('Validation cancelled'))
          }
        })
      })
    },
  },
}
