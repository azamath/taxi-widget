/* eslint-disable */
const VUE_APP_CDN_HOST = process.env.VUE_APP_CDN_HOST;

function appendElement(src, type) {
  var selector = type === 'css' ? "link[href^=\"".concat(src, "\"]") : "script[src^=\"".concat(src, "\"]");
  var isExists = !!document.querySelector(selector);
  if (isExists) return;
  var el = null;

  if (type === 'css') {
    el = document.createElement('link');
    el.setAttribute('href', src);
    el.setAttribute('type', 'text/css');
    el.setAttribute('rel', 'stylesheet');
  } else {
    el = document.createElement('script');
    el.setAttribute('src', src);
    el.setAttribute('type', 'text/javascript');
  }

  document.head.prepend(el);
}

(function () {
  var iviewCss = `${VUE_APP_CDN_HOST}/widget/dist/at-widget-booking.css`;
  var vueJs = 'https://cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js';
  var customElemPolifyll = 'https://cdnjs.cloudflare.com/ajax/libs/custom-elements/1.3.1/custom-elements.min.js'
  var elementJs = `${VUE_APP_CDN_HOST}/widget/dist/at-widget-booking.umd.min.js`;
  var customElementsSupported = () => ('customElements' in window);

  appendElement(iviewCss, 'css')

  var loadWidget = function loadWidget() {
    if(!customElementsSupported()) {
       appendElement(customElemPolifyll, 'js');
    }

    let interval = setInterval(() => {
      if (customElementsSupported()) {
        appendElement(elementJs, 'js');
        clearInterval(interval);
      }
    }, 100)
  };

  var loadScriptsOneByOne = function loadScriptsOneByOne() {
    appendElement(vueJs, 'js');

    var loadVueInterval = setInterval(function () {
      if (window.Vue) {
        clearInterval(loadVueInterval);
        loadWidget();
      }
    }, 100);

  };

  loadScriptsOneByOne();


  const loadingCssAnimation = `
    #at-widget-booking-placeholder {
      min-height: 540px;
      /* padding-top: 100%; 1:1 aspect ratio */
      background-color: #F8FBFF;
      border-radius: 20px;
      border: 2px solid #c1e0f0;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .lds-ripple {
      display: inline-block;
      position: relative;
      top: 0;
      left: 0;
      width: 128px;
      height: 128px;
    }
    .lds-ripple div {
      position: absolute;
      border: 4px solid #4DADFB;
      opacity: 1;
      border-radius: 50%;
      animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
    }
    .lds-ripple div:nth-child(2) {
      animation-delay: -0.5s;
    }
    @keyframes lds-ripple {
      0% {
        top: 60px;
        left: 60px;
        width: 0;
        height: 0;
        opacity: 1;
      }
      100% {
        top: -1px;
        left: -1px;
        width: 122px;
        height: 122px;
        opacity: 0;
      }
    }
  `

  function insertStyleSheetRule(ruleText) {
    const sheets = document.styleSheets

    if(sheets.length == 0)
    {
      let style = document.createElement('style')
      style.appendChild(document.createTextNode(''))
      document.head.appendChild(style)
    }

    const sheet = sheets[sheets.length - 1]
    sheet.insertRule(ruleText, sheet.rules ? sheet.rules.length : sheet.cssRules.length)
  }

  /**
   * render placeholder content
   * when widget in banner is still loading
   */
  let domLoaded = false

  function renderPlaceholder() {

    // find a banner embedded widget
    const widgetInBanner = document.querySelector('at-widget-booking')

    // no widget in banner found
    // return!
    if (!widgetInBanner) return

    //Create parent wrapper for widget
    const parent = document.createElement('div');
    widgetInBanner.parentNode.insertBefore(parent, widgetInBanner);
    parent.appendChild(widgetInBanner);

    // create a div element and apply loader class/classes
    const div = document.createElement('div')
    div.setAttribute('id', 'at-widget-booking-placeholder')

    // add extra four div for ring elements
    div.innerHTML = `
      <style>${loadingCssAnimation}</style>
      <!--        <div class="lds-ring">-->
      <!--          <div></div><div></div><div></div><div></div>-->
      <!--        </div>-->
      <div class="lds-ripple"><div></div><div></div></div>
    `

    parent.prepend(div)
  }

  document.addEventListener('DOMContentLoaded', e => {
    if(!domLoaded) {
      renderPlaceholder()
      domLoaded = true
    }
  })
})();
