import { bounds } from '../../utils/extractors'
import { Tabs } from '../../utils/constants'

export default {
  fromAirportInfo(state) {
    return state.fromAirportInfo
  },

  toAirportForm(state) {
    return state.toAirportForm
  },

  cityTaxiForm(state) {
    return state.cityTaxiForm
  },
  deliveryForm(state) {
    return state.deliveryForm
  },

  airports(state) {
    return state.airports
  },

  selectedTab(state) {
    return state.selectedTab
  },

  isFromAirport(state) {
    return state.selectedTab === 'from' || state.originType === 'airport'
  },

  hasReturnJourney(state, getters) {
    switch (getters.selectedTab) {
      case 'to':
        return state.toAirportForm.returnJourney
      case 'from':
        return state.fromAirportInfo.returnJourney
      case 'cityTaxi':
        return state.cityTaxiForm.returnJourney
    }
  },

  mapOptions(_, __, rootState) {
    const { latlng = '0,0' } = bounds(rootState).origin || {}

    const [latStr, lngStr] = latlng.split(',')
    const lat = Number(latStr.trim())
    const lng = Number(lngStr.trim())

    return {
      center: { lat, lng },
      zoom: 8,
    }
  },

  mediaProps(state) {
    return state.mediaProps
  },
  isMapVisible(state) {
    return state.map.visible
  },

  isNotDelivery(state, getters) {
    return !getters.isDelivery
  },

  isDelivery(state) {
    return state.selectedTab === Tabs.DELIVERY
  },

  isAirportTabs(state) {
    return state.selectedTab === Tabs.TRANSFER_TO ||
      state.selectedTab === Tabs.TRANSFER_FROM ||
      state.originType === 'airport' ||
      state.destinationType === 'airport'
  },

  visibleTabs(state) {
    return ['from', 'to', 'cityTaxi', 'delivery'].filter(key => {
      if (key === 'from' || key === 'to') {
        return !state.hiddenRideTypes.includes('transfer')
      }
      return !state.hiddenRideTypes.includes(key)
    })
  },
}
