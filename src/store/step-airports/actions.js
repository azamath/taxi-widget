import axios from 'axios/index'

export default {
  fetchAirports({ commit, rootState }) {
    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios
      .get(`${API_ENDPOINT}/booking-process/airports`)
      .then(({ data }) => commit('setAirports', data.data))
      .catch(err => {
        if (err.response && err.response.status === 503) {
          commit('setDisabled', true, { root: true })
        }

        return Promise.reject(err)
      })
  },

  // eslint-disable-next-line no-unused-vars
  setMediaProps({ commit }, width) {
    let prop = { span: '6' }

    if (width <= 300) prop = { span: '24' }
    else if (width <= 470) prop = { span: '12' }
    else if (width <= 650) prop = { span: '8' }
    commit('setMediaProps', prop)
  },
}
