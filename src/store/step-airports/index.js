import actions from './actions'
import getters from './getters'
import defaultState from './state'
import mutations from './mutations'

const namespaced = true

export default {
  actions,
  getters,
  mutations: {
    ...mutations,
    reset(state) {
      Object.assign(state, defaultState())
    },
  },
  namespaced,
  state: defaultState(),
}
