export default () => ({
  selectedTab: 'from',
  originType: null,
  destinationType: null,
  hiddenRideTypes: [],

  airports: [],

  fromAirportInfo: {
    pickupLocation: '',
    dropOffLocation: null,
    dropOffDate: null,
    dropOffTime: null,
    passengers: null,
    returnJourney: null,
    returnDate: null,
    returnTime: null,
    returnPassengers: null,
  },

  toAirportForm: {
    pickupLocation: {
      origin: {
        address: '',
        latlng: '',
      },
    },
    dropOffLocation: {
      origin: {
        address: '',
        latlng: '',
        // maybe other stuff
      },
    },
    date: null,
    time: null,
    passengers: null,
    returnJourney: null,
    returnDate: null,
    returnTime: null,
    returnPassengers: null,
  },
  cityTaxiForm: {
    pickupLocation: {
      origin: {
        address: '',
        latlng: '',
      },
    },
    dropOffLocation: {
      origin: {
        address: '',
        latlng: '',
      },
    },
    date: null,
    time: null,
    passengers: null,
    returnJourney: null,
    returnDate: null,
    returnTime: null,
    returnPassengers: null,
  },

  deliveryForm: {
    pickupLocation: {
      origin: {
        address: '',
        latlng: '',
      },
    },
    dropOffLocation: {
      origin: {
        address: '',
        latlng: '',
      },
    },
    date: null,
    time: null,
  },

  mediaProps: { span: '6' },
  map: {
    visible: false
  }
})
