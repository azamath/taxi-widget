/* eslint-disable no-param-reassign */
export default {
  setAirports(state, airports) {
    state.airports = airports.sort(
      (first, second) => first.order - second.order,
    )
  },

  setFromAirportInfo(state, info) {
    if(info.dropOffDate && !(info.dropOffDate instanceof Date ) ){
      info.dropOffDate = new Date(info.dropOffDate)
    }
    if( info.returnDate && !(info.returnDate instanceof Date)){
      info.returnDate = new Date(info.returnDate)
    }
    state.fromAirportInfo = info
  },

  setSelectedTab(state, tab) {
    state.selectedTab = tab
  },

  setToAirportForm(state, info) {
    if (info.date && !(info.date instanceof Date)) {
      info.date = new Date(info.date)
    }
    if (info.returnDate && !(info.returnDate instanceof Date)) {
      info.returnDate = new Date(info.returnDate)
    }
    state.toAirportForm = info
  },

  setCityTaxiForm(state, info) {
    if (info.date && !(info.date instanceof Date)) {
      info.date = new Date(info.date)
    }
    if (info.returnDate && !(info.returnDate instanceof Date)) {
      info.returnDate = new Date(info.returnDate)
    }
    state.cityTaxiForm = info
  },

  setDeliveryForm(state, info) {
    if (info.date && !(info.date instanceof Date)) {
      info.date = new Date(info.date)
    }

    state.deliveryForm = info
  },

  setMediaProps(state, mediaProps) {
    state.mediaProps = mediaProps
  },

  setIsMapVisible(state, isVisible) {
    state.map.visible = !!isVisible
  },

  setHiddenRideTypes(state, hiddenRideTypes) {
    state.hiddenRideTypes = hiddenRideTypes
  },

  setOriginType(state, originType) {
    state.originType = originType
  },

  setDestinationType(state, type) {
    state.destinationType = type
  },
}
