/* eslint-disable no-param-reassign */
const defaultState = () => ({
  order: null,
  invoiceUrl: '',
  invoiceUrlReturn: '',
})

export default {
  namespaced: true,

  state: defaultState(),

  mutations: {
    setOrder(state, order) {
      state.order = order
    },

    setInvoiceUrl(state, url) {
      state.invoiceUrl = url
    },

    setInvoiceUrlReturn(state, url) {
      state.invoiceUrlReturn = url
    },

    reset(state) {
      Object.assign(state, defaultState())
    },
  },

  getters: {
    getOrderFees(state) {
      return state.order ? (state.order.fees || []) : []
    },
    shouldSkipPayment(_, __, rootState, rootGetters) {
      if (rootGetters['transfer-details/hasPrice']) {
        return rootState.isAdmin || rootState['transfer-details'].payment.deferred
      }
      return true;
    },
  },
}
