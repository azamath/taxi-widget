/* eslint-disable no-param-reassign */
import axios from 'axios'
import Vue from 'vue'

export default {
  namespaced: true,

  state: {
    settings: null,
  },

  mutations: {
    setSettings(state, settings) {
      state.settings = settings
    },
  },

  getters: {
    settings({ settings }) {
      return settings
    },
    paymentMethods({ settings }) {
      return settings.payment_methods
    },
    privacyPolicyText({ settings }) {
      const locale = Vue.prototype.$i18n.locale
      return settings.privacy_full_text && settings.privacy_full_text[locale] || ''
    },
    termsAndConditionsText({ settings }) {
      const locale = Vue.prototype.$i18n.locale
      return settings.term_agree_text && settings.term_agree_text[locale] || ''
    },
  },

  actions: {
    fetchSettings({ commit, rootState }) {
      const { VUE_APP_API_ENDPOINT } = rootState.environment

      return (
        axios
          // prettier-ignore
          .get(`${VUE_APP_API_ENDPOINT}/settings`)
          .then(response => {
            commit('setSettings', response.data)
          })
      )
    },
  },
}
