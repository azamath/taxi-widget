/* eslint-disable no-param-reassign */
export default {
  setPriceDetails(state, priceDetails) {
    state.priceDetails = priceDetails
  },

  setPassengerInformation(state, passengerInformation) {
    // merge results
    state.passengerInformation = {
      ...state.passengerInformation,
      ...passengerInformation,
    }
  },


  setMeetPhoto(state, meetPhoto) {
    state.luggage.meet_photo = meetPhoto
  },

  setBookResponse(state, bookResponse) {
    state.bookResponse = bookResponse
  },

  setDeferredPayments(state, deferredPayments) {
    state.payment.deferred = deferredPayments;
  }
}
