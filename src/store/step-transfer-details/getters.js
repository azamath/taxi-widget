/* eslint-disable camelcase */
export default {
  totalPrice(state) {
    const { calculated_ride = {}, calculated_return_ride = {} } = state

    const { total: totalFirst = 0.0 } = calculated_ride
    const { total: totalSecond = 0.0 } = calculated_return_ride

    return (totalFirst + totalSecond).toFixed(2)
  },

  getPaymentFees(state) {
    if (!state.priceDetails) {
      return []
    }

    const { payment_fees } = state.priceDetails

    return payment_fees[state.payment.method]
  },

  hasPrice(state) {
    const { calculated_ride = {}, calculated_return_ride = {} } = state.priceDetails || {}

    return !!(calculated_ride.total || calculated_return_ride.total)
  },

  isShared(state) {
    return state.isShared || state.returnTransferDetails.isShared
  }
}
