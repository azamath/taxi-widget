import actions from './actions'
import mutations from './mutations'
import defaultState from './state'
import getters from './getters'

const namespaced = true

export default {
  namespaced,
  state: defaultState(),
  actions,
  mutations: {
    ...mutations,
    reset(state) {
      Object.assign(state, defaultState())
    },
  },
  getters,
}
