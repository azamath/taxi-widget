import axios from 'axios'
import extractBookingBody, { extractReturnRoute, extractRoute } from '../../utils/extractors'

import { AT_TOKEN_CREDENTIALS } from '../../utils/constants'
import { EventBus } from '../../utils'

/* eslint-disable no-param-reassign, camelcase */
export default {
  recalculate({ rootState, commit }) {
    // booking body and calculate body are same
    const body = extractBookingBody(rootState)

    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    const { token: authToken } = rootState['transfer-details'].passengerInformation

    return axios
      .post(`${API_ENDPOINT}/booking-process/calculate`, body, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        }
      })
      .then(({ data }) => {
        commit('setPriceDetails', data)
        return data
      })
  },

  fetchRoutePolyline({ rootState, commit }) {
    const route = extractRoute(rootState)
    const return_route = extractReturnRoute(rootState)
    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT

    return axios.post(`${API_ENDPOINT}/booking-process/route`, {
        route,
        return: !!return_route,
        return_route,
      })
      .then(({ data }) => {
        commit('car-classes/setRoute', data.route_info, { root: true })
        commit('car-classes/setReturnRoute', data.return_route_info, { root: true })
        return data
      })
  },

  checkForLogin({ commit, rootState }) {
    if (rootState.isPersonal) {
      // account credentials inserted manually in admin page
      return Promise.resolve()
    }

    const {
      name,
      lastname,
    } = rootState['transfer-details'].passengerInformation

    // user already logged in
    if (name || lastname) return Promise.resolve()

    const token = localStorage.getItem(AT_TOKEN_CREDENTIALS)
    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT

    if (token) {
      return axios
        .get(`${API_ENDPOINT}/account`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then(({ data: { data: user, stripe_key } }) => {
          const { phone, phone_2, email_2 } = user.settings
          commit('setPassengerInformation', {
            title: user.appeal,
            name: user.first_name,
            lastname: user.last_name,
            email: user.email,
            confirmEmail: user.email,
            phone,
            additionalPhone: phone_2,
            additionalEmail: email_2,
            token,
          })
          commit('setDeferredPayments', user.deferred_payments);

          if (stripe_key) {
            commit('environment/set', {
              key: 'VUE_APP_STRIPE_KEY',
              value: stripe_key,
            }, { root: true })
          }

          EventBus.$emit('phone-changed', { phone, phone_2 })
          return user
        })
    }
    return Promise.resolve()
  },

  logout({ commit }) {
    localStorage.removeItem(AT_TOKEN_CREDENTIALS)
    commit('setPassengerInformation', {
      title: 'mr',
      name: '',
      lastname: '',
      email: '',
      confirmEmail: '',
      phone: '',
      additionalPhone: '',
      additionalEmail: '',
      token: '',
    });
    commit('setDeferredPayments', false);
  },

  login({ commit, rootState }, body) {
    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT

    return axios
      .post(`${API_ENDPOINT}/auth/login`, body)
      .then(({ data: { token } }) => {
        localStorage.setItem(AT_TOKEN_CREDENTIALS, token)
        return axios.get(`${API_ENDPOINT}/account`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
      })
      .then(({ data: { data: user, stripe_key } }) => {
        const token = localStorage.getItem(AT_TOKEN_CREDENTIALS)
        const { phone, phone_2, email_2 } = user.settings
        commit('setPassengerInformation', {
          title: user.appeal,
          name: user.first_name,
          lastname: user.last_name,
          email: user.email,
          confirmEmail: user.email,
          phone,
          additionalPhone: phone_2,
          additionalEmail: email_2,
          token,
        })
        commit('setDeferredPayments', user.deferred_payments)

        if (stripe_key) {
          commit('environment/set', {
            key: 'VUE_APP_STRIPE_KEY',
            value: stripe_key,
          }, { root: true })
        }

        EventBus.$emit('phone-changed', { phone, phone_2 })
        return user
      })
  },

  register({ state, commit, rootState }, setUserFromBody = true) {
    const {
      title: appeal,
      name: first_name,
      lastname: last_name,
      email,
      phone,
      additionalPhone: phone_2,
      additionalEmail: email_2,
    } = state.passengerInformation

    const body = {
      appeal,
      first_name,
      last_name,
      email,
      settings: {
        phone,
        phone_2,
        email_2,
      },
    }

    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios
      .post(`${API_ENDPOINT}/auth/register`, body)
      .then(({ data: user }) => {
        if (setUserFromBody) {
          commit('setPassengerInformation', {
            title: user.appeal || 'mr',
            name: user.first_name,
            lastname: user.last_name,
            email: user.email,
            confirmEmail: user.email,
            token: user.token,
          })
        } else {
          const olduser = state.passengerInformation
          olduser.title = user.appeal || 'mr'
          olduser.token = user.token
          commit('setPassengerInformation', olduser)
        }
        return user.token
      })
  },

  book({ rootState, state, commit }) {
    const body = extractBookingBody(rootState)
    const { token } = state.passengerInformation

    if (state.bookResponse) return Promise.resolve(state.bookResponse)

    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios
      .post(`${API_ENDPOINT}/bookings`, body, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then(response => {
        commit('setBookResponse', response)
        const { order, invoice_url, invoice_url_return } = response.data
        commit('payment/setOrder', order, { root: true })
        commit('payment/setInvoiceUrl', invoice_url, { root: true })
        commit('payment/setInvoiceUrlReturn', invoice_url_return, { root: true })

        return response.data
      })
  },

  fetchUserCards({ state, rootState }) {
    const { token } = state.passengerInformation

    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios.get(`${API_ENDPOINT}/cards`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  },

  sendTokenToServer({ state, rootState }, token) {
    const { token: authToken } = state.passengerInformation
    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios.post(
      `${API_ENDPOINT}/cards`,
      { token: token.id },
      {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      },
    )
  },

  clearMeetPhoto({ commit }) {
    commit('setMeetPhoto', null)
  },

  uploadMeetImage({ state, commit, rootState }) {
    const { meet_image } = state.luggage

    if (meet_image) {
      const form = new FormData()
      form.append('image', meet_image)

      const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT

      return axios
        .post(`${API_ENDPOINT}/storage/upload`, form, {
          headers: { 'Content-Type': 'multipart/form-data' },
        })
        .then(({ data }) => {
          commit('setMeetPhoto', data)
          return data
        })
    }

    return Promise.resolve()
  },

  startPayment({ state, rootState }, { method, card_id, order_id }) {
    const { token: authToken } = state.passengerInformation

    const body = {
      method,
      card_id,
    }

    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios.post(`${API_ENDPOINT}/orders/${order_id}/pay`, body, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    })
      .then(({ data }) => data)
  },

  fetchOrder({ state, rootState }, order_id) {
    const { token: authToken } = state.passengerInformation
    const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
    return axios.get(`${API_ENDPOINT}/orders/${order_id}`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      })
      .then(({ data }) => data.data)
  }
}
