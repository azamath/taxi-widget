import axios from "axios";

export default {
  namespaced: true,

  state: {
    trackedEvents: [],
  },

  actions: {
    trackEvent({ commit, state, rootState }, event) {
      if (!state.trackedEvents.includes(event)) {
        const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
        const url = `${API_ENDPOINT}/tracking`
        axios
          .post(url, { event })
          .then(() => {
            commit("trackEvent", event)
          });
      }
    }
  },

  mutations: {
    trackEvent(state, event) {
      state.trackedEvents.push(event)
    }
  }
}
