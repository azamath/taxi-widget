/* eslint-disable no-param-reassign */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import logger from 'vuex/dist/logger'
import clonedeep from 'lodash.clonedeep'

import airports from './step-airports'
import steps from './steps'
import carClasses from './step-car-class'
import transferDetails from './step-transfer-details'
import payment from './step-payment'
import environment from './environment'
import settings from './settings'
import tracking from './tracking'
import { EventBus } from '../utils'

Vue.use(Vuex)

const plugins = []
if (process.env.NODE_ENV !== 'production') plugins.push(logger())

const options = {
  modules: {
    airports,
    steps,
    'car-classes': carClasses,
    'transfer-details': transferDetails,
    environment,
    payment,
    settings,
    tracking,
  },

  plugins,

  state: {
    localeLoaded: false,
    disabled: false,
    isAdmin: false,
    users: [],
    adminSelectedUser: null,
    locales: [],
    couponCode: null,
    currency: {
      symbol: '€',
      short: 'EU',
      full: 'EURO',
    },
    questionMarks: [],
    redirect: null,
    redirectHandled: false,
    blank: false,
    privacyPolicy: null,
    termsAndConditions: null,
    isPersonal: false,
    latestBookings: [],
    partnerId: null,
    paymentRedirect: null,
    showArrivalBooking: false,
    showLatestBookings: true,
  },

  mutations: {
    setTermsAndConditions(state, termsAndConditions) {
      state.termsAndConditions = termsAndConditions
    },

    setPrivacyPolicy(state, privacyPolicy) {
      state.privacyPolicy = privacyPolicy
    },

    setQuestionMarks(state, questionMarks) {
      state.questionMarks = questionMarks
    },

    setLocaleLoaded(state, loaded) {
      state.localeLoaded = loaded
    },

    setDisabled(state, disabled) {
      state.disabled = disabled
    },

    setIsAdmin(state, isAdmin) {
      state.isAdmin = isAdmin
    },

    setIsPersonal(state, isPersonal) {
      state.isPersonal = isPersonal
    },

    setAdminSelectedUser(state, user) {
      state.adminSelectedUser = user
    },

    setLocales(state, locales) {
      state.locales = locales
    },

    setCouponCode(state, couponCode) {
      state.couponCode = couponCode
    },

    setCurrency(state, currency) {
      state.currency = currency
    },

    setRedirect(state, redirect) {
      state.redirect = redirect
    },

    setRedirectHandled(state, redirectHandled) {
      state.redirectHandled = redirectHandled
    },

    setShowArrivalBooking(state, value) {
      state.showArrivalBooking = value
    },

    setBlank(state, blank) {
      state.blank = blank
    },

    setLatestBookings(state, bookings) {
      state.latestBookings = bookings
    },

    setPartnerId(state, partnerId) {
      state.partnerId = partnerId
    },

    setPaymentRedirect(state, url) {
      state.paymentRedirect = url
    },

    setShowLatestBookings(state, value) {
      state.showLatestBookings = value
    },
  },

  actions: {
    fetchLocales({ commit, rootState }) {
      const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
      return axios
        .get(`${API_ENDPOINT}/translations/locales`)
        .then(({ data }) => commit('setLocales', data))
    },

    fetchQuestionMarks({ commit, rootState }) {
      const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
      return axios
        .get(`${API_ENDPOINT}/widget/pairs`)
        .then(({ data }) => commit('setQuestionMarks', data))
    },

    setAdminSelectedUser({ commit, dispatch }, user) {
      // eslint-disable-next-line camelcase
      commit('setAdminSelectedUser', user)
      dispatch('setAccount', user)
    },

    setAccount({ commit }, user) {
      const { phone, phone_2, email_2 } = user.settings
      commit('transfer-details/setPassengerInformation', {
        title: user.appeal,
        name: user.first_name,
        lastname: user.last_name,
        email: user.email,
        confirmEmail: user.email,
        phone,
        additionalPhone: phone_2,
        additionalEmail: email_2,
        token: user.token,
      })
      commit('transfer-details/setDeferredPayments', user.deferred_payments || false);
      EventBus.$emit('phone-changed', { phone, phone_2 })
    },

    resetInitialStore({ commit }) {
      commit('steps/reset')
      commit('airports/reset')
      commit('car-classes/reset')
      commit('transfer-details/reset')
      commit('payment/reset')
    },

    fetchLatestBookings({ commit, rootState }) {
      const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
      return axios
        .get(`${API_ENDPOINT}/widget/bookings/latest`)
        .then(({ data }) => commit('setLatestBookings', data))
    }
  },

  getters: {
    currency(state) {
      return state.currency
    },

    questionMarks(state) {
      return key => state.questionMarks[key]
    },

    redirect(state) {
      return state.redirect
    },

    blank(state) {
      return state.blank
    },

    redirectHandled(state) {
      return state.redirectHandled
    },

    datepickerOptions() {
      return {
        disabledDate(date) {
          return date && date.valueOf() < Date.now() - 86400000
        },
      }
    },

    showArrivalBooking(state) {
      return state.showArrivalBooking
    },
  },
}

export default () => new Vuex.Store(clonedeep(options))
