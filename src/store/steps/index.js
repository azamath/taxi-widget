/* eslint-disable no-param-reassign */
const defaultState = () => ({
  step: 0,
  takenSteps: [0],
})

export default {
  namespaced: true,

  state: defaultState(),

  mutations: {
    nextStep(state) {
      const step = state.step + 1
      state.step = step

      if (!state.takenSteps.includes(step)) {
        state.takenSteps.push(step)
      }
    },

    previousStep(state) {
      state.step -= 1
    },

    toStep(state, step) {
      state.step = step
    },

    setStep(state, step) {
      state.step = step
    },

    reset(state) {
      Object.assign(state, defaultState())
    },
  },

  actions: {
    nextStep({ commit }) {
      commit('nextStep')
    },

    previousStep({ commit }) {
      commit('previousStep')
    },

    toStep({ commit, state }, step) {
      if (state.takenSteps.includes(step)) commit('toStep', step)
    },

    reset({ commit }) {
      commit('reset')
    },
  },

  getters: {
    step(state) {
      return state.step
    },
  },
}
