/* eslint-disable no-param-reassign */
import axios from 'axios'

import { prepareBody } from '../../utils'

const defaultState = () => ({
  vehicles: [],
  route: [],
  returnRoute: [],
  // user selected vehicle
  vehicle: null,
})

export default {
  namespaced: true,

  state: defaultState(),

  mutations: {
    setVehicles(state, vehicles) {
      state.vehicles = vehicles.sort(
        (first, second) => first.order - second.order,
      )
    },

    setRoute(state, route) {
      state.route = route
    },

    setReturnRoute(state, returnRoute) {
      state.returnRoute = returnRoute
    },

    setVehicle(state, vehicle) {
      state.vehicle = vehicle
    },

    reset(state) {
      Object.assign(state, defaultState())
    },
  },

  getters: {
    vehicles(state) {
      return state.vehicles || []
    },

    route(state) {
      return state.route || []
    },
    returnRoute(state) {
      return state.returnRoute || []
    },
    vehicle(state) {
      return state.vehicle
    },

    vehicleHas(state) {
      return prop => {
        // todo: temporary solution to use not existent property
        //       because features property is overwritten
        return state.vehicle.options && state.vehicle.options.includes(prop)
      }
    },
  },

  actions: {
    fetchVehicles({ commit, rootState }) {
      const API_ENDPOINT = rootState.environment.VUE_APP_API_ENDPOINT
      const url = `${API_ENDPOINT}/booking-process/vehicles`

      const body = prepareBody(rootState)

      return axios.post(url, body).then(({ data }) => {
        commit('setVehicles', data.data)
        commit('airports/setOriginType', data.origin_type || null, { root: true })
        commit('airports/setDestinationType', data.destination_type || null, { root: true })
        return data
      })
    },

    setVehicle({ commit, rootState }, vehicle) {
      commit('setVehicle', vehicle)
      /* eslint-disable */
      const {
        calculated: calculated_ride,
        calculated_return: calculated_return_ride,
        payment_fees,
        shared_id,
        return_shared_id,
      } = vehicle
      /* eslint-enable */

      commit(
        'transfer-details/setPriceDetails',
        {
          calculated_ride,
          calculated_return_ride,
          // eslint-disable-next-line camelcase
          payment_fees: payment_fees || [],
        },
        { root: true },
      )
      rootState['transfer-details'].sharingJoinTo = shared_id
      rootState['transfer-details'].returnTransferDetails.sharingJoinTo = return_shared_id
    },
  },
}
