export default {
  namespaced: true,

  state: {
    VUE_APP_GOOGLE_MAP_KEY: null,
    VUE_APP_API_ENDPOINT: null,
    VUE_APP_STRIPE_KEY: null,
  },

  mutations: {
    set(state, { key, value }) {
      state[key] = value // eslint-disable-line
    },
  },

  getters: {
    get(state) {
      return key => state[key]
    },
  },
}
