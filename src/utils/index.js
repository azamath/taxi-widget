/* eslint-disable camelcase, prefer-destructuring */
import Vue from 'vue'
import moment from 'moment'
import qs from 'qs'

export const EventBus = new Vue()

// eslint-disable-next-line
export const loadGoogleMapJs = (
  libs,
  apiKey = process.env.VUE_APP_GOOGLE_MAP_KEY,
) => {
  let interval = null
  // eslint-disable-next-line
  return new Promise(resolve => {
    const { google } = window

    /* eslint-ignore */
    /* prettier-ignore */
    const condition = google
      && google.maps
      && google.maps.places
      && google.maps.places.Autocomplete

    if (condition) {
      resolve()
      return
    }

    let scriptSrc = `https://maps.googleapis.com/maps/api/js?key=${apiKey}`

    if (libs && libs.length) {
      scriptSrc = `${scriptSrc}&libraries=${libs.join()}`
    }

    const scripts = document.querySelectorAll(`script[src^="${scriptSrc}"]`)

    if (!scripts || !scripts.length) {
      // no google maps script tag added to DOM
      // so insert it manually

      if (!apiKey) {
        // prettier-ignore
        // eslint-ignore
        const message = 'Invalid Google Maps API key. Is VUE_APP_GOOGLE_MAP_KEY variable defined in .env.* file?'
        // eslint-disable-next-line
        console.warn(message)
        resolve()
        return
      }

      // create script with src and insert it
      const scriptEl = document.createElement('script')
      scriptEl.setAttribute('src', scriptSrc)
      document.head.appendChild(scriptEl)
    }

    // google is not defined/loaded yet
    // so insert script element
    // and wait for global "window.google" variable
    interval = setInterval(() => {
      // prettier-ignore
      const condition2 = window.google
        && window.google.maps
        && window.google.maps.places
        && window.google.maps.places.Autocomplete

      if (condition2) {
        clearInterval(interval)
        resolve()
      }
    }, 500)
  })
}

export const loadStripeJs = () => {
  let interval = null
  // eslint-disable-next-line
  return new Promise(resolve => {
    if (window.Stripe) {
      resolve()
      return
    }

    const scripts = document.querySelectorAll(
      'script[src^="https://js.stripe.com/v3/"]',
    )

    if (!scripts || !scripts.length) {
      // no Stripe script tag added to DOM
      // so insert it manually
      const scriptSrc = 'https://js.stripe.com/v3/'

      // create script with src and insert it
      const scriptEl = document.createElement('script')
      scriptEl.setAttribute('src', scriptSrc)
      document.head.appendChild(scriptEl)
    }

    // Stripe is not defined/loaded yet
    // so insert script element
    // and wait for global "window.Stripe" variable
    interval = setInterval(() => {
      if (window.Stripe) {
        clearInterval(interval)
        resolve()
      }
    }, 500)
  })
}

export const getISODate = (dateStr, timeStr) => {
  const time = moment(timeStr, 'HH:mm').format('HH:mm')
  const date = new Date(dateStr)
  const [hour, minute] = time.replace(/ /g, '').split(':')
  date.setHours(Number(hour), Number(minute))
  return moment(date).format('YYYY-MM-DD HH:mm')
}

export const extractAirportId = ({ airports }) => {
  const {
    fromAirportInfo: { pickupLocation = {} },
    toAirportForm: { pickupLocation: p = {} },
  } = airports

  const origin_id = pickupLocation.id
  const destination_id = p.id

  return { origin_id, destination_id }
}

const prepareBodyFromTo = (rootState, stops, animals) => {
  const {
    pickupLocation,
    dropOffLocation,
    date: dateStr,
    time,
    passengers,
    returnJourney,
    returnDate,
    returnTime,
  } = rootState.airports.toAirportForm

  const date = getISODate(dateStr, time)

  const result = {
    pickup_date: date,
    route: {
      origin: dropOffLocation.origin,
      destination: pickupLocation.origin,
    },
    details: {
      passengers,
      animals,
    },
    is_shared: rootState['transfer-details'].isShared,
  }

  const { destination_id } = extractAirportId(rootState)
  result.destination_id = destination_id

  if (stops && stops.length) {
    result.route.stops = stops
  }

  if (returnJourney) {
    result.return = true
    result.return_pickup_date = getISODate(returnDate, returnTime)
  }

  return result
}

const prepareBodyFromFrom = (rootState, stops, animals) => {

  const {
    dropOffLocation,
    pickupLocation,
    dropOffTime,
    dropOffDate,
    passengers,
    returnJourney,
    returnDate,
    returnTime,
  } = rootState.airports.fromAirportInfo

  const date = getISODate(dropOffDate, dropOffTime)

  const result = {
    pickup_date: date,
    route: {
      origin: {
        address: pickupLocation.name,
        latlng: pickupLocation.location.latlng,
      },
      destination: {
        address: dropOffLocation.name,
        latlng: dropOffLocation.destination,
      },
    },
    details: {
      passengers,
      animals,
    },
    is_shared: rootState['transfer-details'].isShared,
  }

  const { origin_id } = extractAirportId(rootState)
  result.origin_id = origin_id

  if (stops && stops.length) {
    result.route.stops = stops
  }

  if (returnJourney) {
    result.return = true
    result.return_pickup_date = getISODate(returnDate, returnTime)
  }

  return result
}

const prepareBodyCityTaxi = (rootState, stops, animals) => {
  let {
    pickupLocation,
    dropOffLocation,
    date,
    time,
    passengers,
    returnJourney,
    returnDate,
    returnTime,
  } = rootState.airports.cityTaxiForm

  date = getISODate(date, time)

  const result = {
    pickup_date: date,
    route: {
      origin: pickupLocation.origin,
      destination: dropOffLocation.origin,
    },
    details: {
      passengers,
      animals,
    },
    is_shared: rootState['transfer-details'].isShared,
  }

  if (stops && stops.length) {
    result.route.stops = stops
  }

  if (returnJourney) {
    result.return = true
    result.return_pickup_date = getISODate(returnDate, returnTime)
  }

  return result
}

const prepareBodyDelivery = (rootState) => {
  let {
    pickupLocation,
    dropOffLocation,
    date,
    time,
  } = rootState.airports.deliveryForm

  date = getISODate(date, time)

  return {
    pickup_date: date,
    route: {
      origin: pickupLocation.origin,
      destination: dropOffLocation.origin,
    },
    details: {},
  }
}

export const prepareBody = (rootState, stops = [], animals = []) => {
  const { selectedTab } = rootState.airports

  // search action called from to tab
  let result
  switch (selectedTab) {
    case 'to':
      result = prepareBodyFromTo(rootState, stops, animals)
      result.ride_type = 'transfer'
      break;
    case 'from':
      result = prepareBodyFromFrom(rootState, stops, animals)
      result.ride_type = 'transfer'
      break;
    case 'cityTaxi':
      result = prepareBodyCityTaxi(rootState, stops, animals)
      result.ride_type = 'city_taxi'
      break;
    case 'delivery':
      result = prepareBodyDelivery(rootState, stops, animals)
      result.ride_type = 'delivery'
      break;
    default:
      throw new Error(`Preparing body from ${selectedTab} tab is undefined`)
  }

  return result;
}

export const getFromShadowDom = id => {
  const root = document.querySelector('at-widget-booking')
  return root ? root.shadowRoot.querySelector(`#${id}`) : null
}

export const getAddressDetails = place => {
  const details = {}
  if (place.address_components) {
    place.address_components.forEach(component => {
      if (component.types.indexOf('country') !== -1) {
        details.country = component.short_name
      }
      if (component.types.indexOf('administrative_area_level_1') !== -1) {
        details.region = component.short_name
      }
      if (component.types.indexOf('locality') !== -1) {
        details.city = component.short_name
      }
      if (component.types.indexOf('postal_code') !== -1) {
        details.zip = component.short_name
      }
    })
  }
  return details
}

export const scrollToSelectorOrElement = selector => {
  if (selector instanceof HTMLElement) {
    selector.scrollIntoView({ behavior: 'smooth' })
  } else {
    const el = document.querySelector(selector)
    if (el) el.scrollIntoView({ behavior: 'smooth' })
    else console.warn(`Can't find element with selector ${selector}`)
  }
}

export const parseQuery = search => {
  const params = qs.parse(search.replace('?', ''), {
    decoder(str, decoder, charset) {
      const strWithoutPlus = str.replace(/\+/g, ' ')
      if (charset === 'iso-8859-1') {
        // unescape never throws, no try...catch needed:
        return strWithoutPlus.replace(/%[0-9a-f]{2}/gi, unescape)
      }

      if (/^(\d+|\d*\.\d+)$/.test(str)) {
        return parseFloat(str)
      }

      const keywords = {
        true: true,
        false: false,
        null: null,
        undefined,
      }
      if (str in keywords) {
        return keywords[str]
      }

      // utf-8
      try {
        return decodeURIComponent(strWithoutPlus)
      } catch (e) {
        return strWithoutPlus
      }
    },
  })
  return params
}

export function isValidAddressType(types) {
  const validTypes = [
    'establishment',
    'point_of_interest',
    'street_number',
  ]
  return types.some(item => validTypes.includes(item))
}

/**
 * Check that some required address component types from google responses are need to be specified
 *
 * @param {Array} types
 * @return {Boolean}
 */
export function checkNeedSpecifyAddress(types) {
  const streetTypes = ['street_address', 'route'];
  if (types.some(type => streetTypes.includes(type))) {
    return types.indexOf('street_number') === -1
  }
  return false;
}

/**
 * Function will check fresh order information for some statuses
 * and resolve/reject promise if status does not pass the check.
 * It performs 3 http api requests in 3 seconds interval.
 *
 * @param orderFetcher Function that resolves the order
 * @param {Array} acceptStatuses List of statuses to check
 * @return {Promise<any>}
 */
export default function orderChecker(orderFetcher, acceptStatuses = []) {
  return new Promise((resolve, reject) => {
    async function fetchAndCheck(iteration = 1) {
      const order = await orderFetcher()
      if (acceptStatuses.indexOf(order.status) !== -1) {
        resolve(order)
        return
      }

      if (iteration >= 3) {
        reject({
          message: Vue.$t('orders.messages.payment_failed'),
        })
      } else {
        setTimeout(() => fetchAndCheck(iteration + 1), 3000)
      }
    }

    fetchAndCheck()
  })
}
