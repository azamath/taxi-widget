import geoip from './geoip_providers/geojs'

export default {
  ipLookup() {
    return geoip.fetch()
  }
}
