/* eslint-disable import/prefer-default-export */
export const API_ENDPOINT = process.env.VUE_APP_API_ENDPOINT
export const AT_TOKEN_CREDENTIALS = '576d44db-0dee-4559-a728-54f38c1e30c3'

export const RideType = {
  TRANSFER: 'transfer',
  CITY_TAXI: 'city_taxi',
  DELIVERY: 'delivery',
}
export const Tabs = {
  TRANSFER_FROM: 'from',
  TRANSFER_TO: 'to',
  CITY_TAXI: 'cityTaxi',
  DELIVERY: 'delivery',
}
