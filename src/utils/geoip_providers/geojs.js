export default {
  fetch() {
    return fetch('https://get.geojs.io/v1/ip/geo.json')
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        return Promise.reject()
      })
      .then(json => {
        return {
          country: json.country,
          city: json.city,
          region: json.region,
          lat: parseFloat(json.latitude),
          lng: parseFloat(json.longitude),
        }
      })
  }
}
