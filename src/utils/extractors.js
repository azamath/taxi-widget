/* eslint-disable camelcase, prefer-destructuring */
import { extractAirportId, getISODate } from './index'
import { RideType, Tabs } from '@/utils/constants'

const extractDetails = rootState => {
  let passengers = 0
  const { selectedTab, originType, destinationType } = rootState.airports
  if (selectedTab === 'from') {
    passengers = rootState.airports.fromAirportInfo.passengers
  } else if (selectedTab === 'to') {
    passengers = rootState.airports.toAirportForm.passengers
  } else if (selectedTab === 'cityTaxi') {
    passengers = rootState.airports.cityTaxiForm.passengers
  }

  const {
    animals,
    handbagOnly,
    ski_equipment_fee,
    extras = [],
    nameplateText: meet_text,
    meet_greet,
    meet_photo,
    waiting_time,
    door_to_door,
    door_to_door_details,
    loaders,
    recipient,
  } = rootState['transfer-details'].luggage

  let details = null
  if (handbagOnly) {
    details = { passengers }
  } else {
    const detExtra = {}
    extras.forEach(extra => {
      if (extra.name && extra.quantity) detExtra[extra.prop] = extra.quantity
    })

    details = {
      animals,
      passengers,
      ski_equipment: ski_equipment_fee,
      waiting_time,
      door_to_door,
      door_to_door_details,
      loaders,
      recipient,
      ...detExtra,
    }
  }

  const {
    title: client_appeal,
    name: client_first_name,
    lastname: client_last_name,
    email: client_email,
    additionalEmail: client_email_2,
    additionalPhone: client_phone_2,
    phone: client_phone,
    notes,
  } = rootState['transfer-details'].passengerInformation

  let tranferDetails = null
  if (selectedTab === Tabs.TRANSFER_FROM || originType === 'airport') {
    const {
      flightNumber: flight_number,
      airlineName: flight_airlines,
      planeArrivingFrom: flight_from,
      hotelPropertyName: hotel_name,
    } = rootState['transfer-details'].formTransferDetails

    tranferDetails = {
      flight_number,
      flight_airlines,
      flight_from,
      hotel_name,
    }
  } else if (selectedTab === Tabs.TRANSFER_TO || destinationType === 'airport') {
    const {
      time: flight_departure,
      planeDepartingTo: flight_to,
      airlineName: flight_airlines,
      flightNumber: flight_number,
    } = rootState['transfer-details'].toTransferDetails

    tranferDetails = {
      flight_departure,
      flight_number,
      flight_airlines,
      flight_to,
    }
  }

  details = {
    ...details,
    ...{
      client_appeal,
      client_first_name,
      client_last_name,
      client_email,
      client_email_2,
      client_phone,
      client_phone_2,
      meet_text,
      meet_greet,
      meet_photo,
      ...tranferDetails,
    },
    notes,
  }

  return details
}

export const bounds = rootState => {
  let origin = null
  let destination = null

  try {
    if (rootState.airports.selectedTab === 'from') {
      const {
        dropOffLocation,
        pickupLocation,
      } = rootState.airports.fromAirportInfo
      origin = {
        address: pickupLocation.name,
        latlng: pickupLocation.location.latlng,
        ...pickupLocation.location,
      }

      destination = {
        address: dropOffLocation.name,
        latlng: dropOffLocation.destination,
        ...dropOffLocation,
      }
    } else if (rootState.airports.selectedTab === 'to') {
      const {
        pickupLocation,
        dropOffLocation,
      } = rootState.airports.toAirportForm
      origin = dropOffLocation.origin
      destination = pickupLocation.origin
    } else if (rootState.airports.selectedTab === 'cityTaxi') {
      const {
        pickupLocation,
        dropOffLocation,
      } = rootState.airports.cityTaxiForm
      origin = pickupLocation.origin
      destination = dropOffLocation.origin
    } else if (rootState.airports.selectedTab === 'delivery') {
      const {
        pickupLocation,
        dropOffLocation,
      } = rootState.airports.deliveryForm
      origin = pickupLocation.origin
      destination = dropOffLocation.origin
    } else {
      console.error('Unsupported bounds type')
    }
  } catch (err) {
    console.warn(err.message)
  }

  return {
    origin,
    destination,
  }
}

export const routeBounds = (rootState, google) => {
  const { origin, destination } = bounds(rootState)
  const bound = new google.maps.LatLngBounds()

  // eslint-disable-next-line
  ;[origin, destination].forEach(({ latlng }) => {
    const [lat, lng] = latlng.split(',')
    bound.extend(new google.maps.LatLng(parseInt(lat, 10), parseInt(lng, 10)))
  })

  return bound
}

export const extractRoute = rootState => {
  const { origin, destination } = bounds(rootState)

  const { stops } = rootState['transfer-details'].luggage

  return {
    origin,
    destination,
    stops,
  }
}

export const extractOrderedAt = rootState => {
  const { selectedTab } = rootState.airports
  if (selectedTab === 'from') {
    const { dropOffTime, dropOffDate } = rootState.airports.fromAirportInfo
    return getISODate(dropOffDate, dropOffTime)
  } if (selectedTab === 'to') {
    const { date, time } = rootState.airports.toAirportForm
    return getISODate(date, time)
  } if (selectedTab === 'cityTaxi') {
    const { date, time } = rootState.airports.cityTaxiForm
    return getISODate(date, time)
  } if (selectedTab === 'delivery') {
    const { date, time } = rootState.airports.deliveryForm
    return getISODate(date, time)
  }

}

export const extractReturnRoute = rootState => {
  let obj = null
  const { selectedTab } = rootState.airports

  if (selectedTab === 'from') {
    obj = rootState.airports.fromAirportInfo
  } else if (selectedTab === 'to') {
    obj = rootState.airports.toAirportForm
  } else if (selectedTab === 'cityTaxi') {
    obj = rootState.airports.cityTaxiForm
  } else if (selectedTab === 'delivery') {
    return null
  }

  const { returnJourney } = obj
  if (!returnJourney) return null

  const { stops } = rootState['transfer-details'].returnTransferDetails
  const state = rootState['transfer-details']

  const return_route = {
    stops,
  }
  if (state.formTransferDetails.isDifferentPickupAddress) {
    let field = selectedTab === 'from' || selectedTab === 'cityTaxi' ? 'origin' : 'destination'
    const location = state.returnTransferDetails.pickupAddress[field]
    if (location && location.address && location.latlng) {
      return_route[field] = location
    }
  }

  return return_route
}

const extractReturnDetails = rootState => {
  const { selectedTab } = rootState.airports
  let passengers = 0
  if (selectedTab === 'from') {
    passengers = rootState.airports.fromAirportInfo.returnPassengers
  } else if (selectedTab === 'to') {
    passengers = rootState.airports.toAirportForm.returnPassengers
  } else if (selectedTab === 'cityTaxi') {
    passengers = rootState.airports.cityTaxiForm.returnPassengers
  } else if (selectedTab === 'delivery') {
    return {}
  }

  const {
    returnTransferDetails: {
      extras,
      animals,
      waiting_time,
      planeDepartingFrom: flight_from,
      planeDepartingTo: flight_to,
      airlineName: flight_airlines,
      departureFlightNumber: flight_number,
      flightDepartureTime: flight_departure,
    },
    luggage: { isDifferentLuggageOnReturn, handbagOnly },
  } = rootState['transfer-details']

  if (isDifferentLuggageOnReturn) return { passengers }

  const detExtra = {}
  extras.forEach(extra => {
    if (extra.name && extra.quantity) detExtra[extra.prop] = extra.quantity
  })

  return {
    animals,
    passengers,
    waiting_time,
    ...(
      selectedTab === 'from'
        ? {
          flight_to,
          flight_departure,
        } : {
          flight_from,
        }
    ),
    flight_airlines,
    flight_number,
    handbag: handbagOnly,
    is_shared: rootState['transfer-details'].returnTransferDetails.isShared,
    ...detExtra,
  }
}

export const extractReturnOrderedAt = rootState => {
  const { selectedTab } = rootState.airports

  let obj = null
  if (selectedTab === 'from') {
    obj = rootState.airports.fromAirportInfo
  } else if (selectedTab === 'to') {
    obj = rootState.airports.toAirportForm
  } else if (selectedTab === 'cityTaxi') {
    obj = rootState.airports.cityTaxiForm
  } else if (selectedTab === 'delivery') {
    return null
  }

  const { returnDate, returnTime, returnJourney } = obj
  if (!returnJourney) return null

  return getISODate(returnDate, returnTime)
}

export default rootState => {
  const vehicle_id = rootState['car-classes'].vehicle.id
  const {
    payment,
    isShared: is_shared,
    sharingJoinTo: sharing_join_to,
  } = rootState['transfer-details']

  const details = extractDetails(rootState)
  const route = extractRoute(rootState)
  const pickup_date = extractOrderedAt(rootState)

  const return_route = extractReturnRoute(rootState)
  const return_details = extractReturnDetails(rootState)
  const return_pickup_date = extractReturnOrderedAt(rootState)
  const { origin_id, destination_id } = extractAirportId(rootState)

  let result = {
    vehicle_id,
    details,
    route,
    pickup_date,
    payment,
    is_shared,
    sharing_join_to,
  }

  // prettier-ignore
  const hasReturn = rootState.airports.fromAirportInfo.returnJourney
    || rootState.airports.toAirportForm.returnJourney
    || rootState.airports.cityTaxiForm.returnJourney

  const { selectedTab } = rootState.airports
  if (hasReturn) {
    result = {
      ...result,
      return_pickup_date,
      return_route,
      return_details,
      return_sharing_join_to: rootState['transfer-details'].returnTransferDetails.sharingJoinTo,
    }
  }

  if (selectedTab === 'from') {
    result.origin_id = origin_id
    result.ride_type = RideType.TRANSFER
  } else if (selectedTab === 'to') {
    result.destination_id = destination_id
    result.ride_type = RideType.TRANSFER
  } else if (selectedTab === Tabs.CITY_TAXI) {
    result.ride_type = RideType.CITY_TAXI
  } else if (selectedTab === Tabs.DELIVERY) {
    result.ride_type = RideType.DELIVERY
  }

  if (rootState.isAdmin && rootState.adminSelectedUser) {
    result.owner_id = rootState.adminSelectedUser.id
  }
  result.partner_id = rootState.partnerId
  result.coupon_code = rootState.couponCode

  return result
}
