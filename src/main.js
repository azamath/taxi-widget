/* eslint-disable */
import Vue from 'vue'
import Component from './Component.vue'
import './assets/scss/main.less'
import moment from 'moment'

new Vue({
  render(h) {
    return h(Component, {
      props: {
        isAdmin: false,
        // redirect: 'widget-container',
        blank: true,
      },
      ref: 'widget',
    })
    // return h('at-widget-booking', {
    //   attrs: {
    //     endpoint: 'http://taxi.loc/api',
    //   },
    // })
  },
  mounted() {
    // CODE FOR TESTING STEPS
    // UNCOMMENT REQUIRED LINES BELOW

    // const $store = this.$refs.widget.$store
    // $store.commit('settings/setSettings', {
    //   booking_cash_enabled: true,
    // })
    // const airport = {
    //   address: "Brussels airport",
    //   name: "Brussels airport",
    //   "city": "Zaventum",
    //   "region": "Vlaanderen",
    //   "country": "BE",
    //   "latlng": "50.8964267,4.4816194",
    //   "zoom": 12
    // }
    // const location = {
    //   address: "Hotel Metropole, Place De Brouckère, Brussels, Belgium",
    //   name: "Hotel Metropole, Place De Brouckère, Brussels, Belgium",
    //   "latlng": "50.851502,4.353512000000023",
    //   "city": "Bruxelles",
    //   "region": "Bruxelles",
    //   "country": "BE",
    //   "zip": "1000",
    //   types: [
    //     'establishment',
    //     'point_of_interest',
    //     'street_number',
    //   ]
    // }
    // const returnData = {
    //   "returnJourney": true,
    //   "returnDate": moment().add(6, 'd').toDate(),
    //   "returnTime": '01:00',
    //   "returnPassengers": null
    // }

    // $store.commit('airports/setToAirportForm', {
    //   "pickupLocation": {
    //     "id": 1,
    //     "origin": airport
    //   },
    //   "dropOffLocation": {
    //     "origin": location
    //   },
    //   "date": moment().add(3, 'd').toDate(),
    //   "time": "01:00",
    //   "passengers": 1,
    //
    //   // with return
    //   // ...returnData
    // })

    // $store.commit('airports/setSelectedTab', 'from')
    // $store.commit('airports/setFromAirportInfo', {
    //   pickupLocation: {
    //     id: 1,
    //     name: airport.address,
    //     location: airport
    //   },
    //   dropOffLocation: {
    //     ...location,
    //     destination: location.latlng,
    //   },
    //   "dropOffDate": moment().add(3, 'd').toDate(),
    //   "dropOffTime": "01:00",
    //   "passengers": 1,
    //
    //   // with return
    //   // ...returnData
    // })

    // $store.commit('steps/nextStep')
    // $store.state['transfer-details'].toTransferDetails = {
    //   planeDepartingTo: 'NY',
    //   airlineName: 'NY airlines',
    //   flightNumber: 'NY273645',
    //   time: '13:00',
    // }
    // $store.state['transfer-details'].formTransferDetails = {
    //   airlineName: 'Fly Emirates',
    //   flightNumber: 'FL42387546',
    //   planeArrivingFrom: 'Dubai',
    //   // isDifferentPickupAddress: true,
    // }
    // $store.state['transfer-details'].returnTransferDetails = {
    //   ...$store.state['transfer-details'].returnTransferDetails,
    //   departureFlightNumber: 'NY273645',
    //   planeDepartingFrom: 'NY',
    //   planeDepartingTo: 'NY',
    //   airlineName: 'NY airlines',
    // }
    // $store.dispatch('transfer-details/login', {
    //   "email": "client@taxi.loc",
    //   "password": "secret",
    // })
    // $store.commit('transfer-details/setPassengerInformation', {
    //   "title": "ms",
    //   "name": "Imogene",
    //   "lastname": "Auer",
    //   "email": "hotel@brussels.be",
    //   "confirmEmail": "hotel@brussels.be",
    //   "phone": "+998909009090",
    //   "additionalPhone": "",
    //   "additionalEmail": "anissa.turcotte@yahoo.com",
    //   "notes": ""
    // })

    // $store.commit('transfer-details/setPriceDetails', {
    //   "calculated_ride": {
    //     "transfer": 50,
    //     "extra": {"stops_fee": 0, "passenger_fee": null},
    //     "total": 50,
    //     "deposit": 0,
    //     "payment_fees": {"stripe": {"transaction_fee": 0}, "cash": []},
    //     "currency": "EUR",
    //     "distance": 10420,
    //     "time": 1474,
    //     "quantity_pricing_map": {
    //       "stops_fee": {"quantity": 0, "stock_price": 10},
    //       "passenger_fee": {"quantity": 1, "stock_price": null},
    //       "animal_fee": {"quantity": 0, "stock_price": null}
    //     }
    //   },
    //   payment_fees: {
    //     stripe: [
    //       { description: 'orders.fees.stripe_fee', amount: 10.10 }
    //     ],
    //     cash: []
    //   }
    // })

    // $store.commit('steps/nextStep')
    // $store.commit('payment/setOrder', {
    //   fees: [
    //     { description: 'orders.fees.stripe_fee', amount: 10.10 }
    //   ],
    // })
  },
}).$mount('#app')
