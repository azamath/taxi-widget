import Vue from 'vue'
import VueCustomElement from 'vue-custom-element'
import Component from './Component.vue'

Vue.use(VueCustomElement)

Vue.customElement('at-widget-booking', Component)

// this export is used in taxi-app as widget component (async component)
export { Component }
