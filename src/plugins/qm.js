import Vue from 'vue'

Vue.mixin({
  methods: {
    $qm(key) {
      return {
        enabled:
          // eslint-disable-next-line
          this.$store.getters.questionMarks(key) &&
          this.$te(`widget.question_marks.${key}`, this.$i18n.locale),
        text: this.$t(`widget.question_marks.${key}`),
      }
    },
  },
})
