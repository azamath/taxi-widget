import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Axios from 'axios'
import de from 'iview/dist/locale/de-DE'
import en from 'iview/dist/locale/en-US'
import es from 'iview/dist/locale/es-ES'
import fr from 'iview/dist/locale/fr-FR'
import it from 'iview/dist/locale/it-IT'
import nl from 'iview/dist/locale/nl-NL'
import pt from 'iview/dist/locale/pt-BR'
import ru from 'iview/dist/locale/ru-RU'
import { EventBus } from '../utils'

// Bind i18n to Vue.
// eslint-disable-next-line
let i18n
let endPoint

export const setEndpoint = endpoint => {
  endPoint = endpoint
}

// const locale = document.documentElement.lang || 'en'
const loadedLanguages = []
export const messageGroups = []

// General message group for general purposes
messageGroups.push('general')
messageGroups.push('widget')
messageGroups.push('orders')
messageGroups.push('enums')

/**
 * Check is a translation plugin doesn't define yet
 */
const inApp = !!Vue.prototype.$i18n
if (!inApp) {
  Object.defineProperty(Vue.prototype, '$i18n', {
    get() {
      return i18n
    },
  })

  Vue.use(VueI18n)

  // Create VueI18n instance with options
  i18n = new VueI18n({
    locale: process.env.VUE_APP_I18N_LOCALE,
    fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE,
    silentFallbackWarn: true,
    silentTranslationWarn: true,
    messages: {},
  })
}
else {
  i18n = Vue.prototype.$i18n
}

const iviewLocales = {
  de,
  en,
  es,
  fr,
  it,
  nl,
  pt,
  ru,
}

Object.keys(iviewLocales).forEach(locale => {
  // iview has only translations in "i" key
  i18n.setLocaleMessage(locale, {
    ...i18n.messages[locale] || {},
    i: iviewLocales[locale].i,
  })
})

export function configureLocale(locale) {
  if (inApp) {
    return locale
  }

  i18n.locale = locale
  // Add current locale header to all requests
  Axios.defaults.headers.common['X-Locale'] = locale
  Axios.defaults.headers.common['Accept-Language'] = locale
  document.querySelector('html').setAttribute('lang', locale)
  return locale
}

export function setCurrentLocale(locale) {
  configureLocale(locale)

  if (loadedLanguages.includes(locale)) {
    EventBus.$emit('locale:loaded', true)
    return Promise.resolve(locale)
  }

  return Axios.get(
    // prettier-ignore
    `${endPoint}/translations/export?groups=${messageGroups.join(',')}&locale=${locale}`,
  ).then(({ data }) => {
    i18n.setLocaleMessage(locale, { ...i18n.messages[locale], ...data })
    loadedLanguages.push(locale)
    EventBus.$emit('locale:loaded', true)
    return locale
  })
}

export default i18n

EventBus.$on('locale:changed', setCurrentLocale)

// Allow global access
Vue.$i18n = i18n
Vue.$t = (...args) => {
  return i18n.t(...args)
}
Vue.$tc = (...args) => {
  return i18n.tc(...args)
}
