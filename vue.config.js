module.exports = {
  productionSourceMap: false,
  filenameHashing: false,

  devServer: {
    disableHostCheck: true,
  },

  css: {
    extract: true,
    loaderOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },

  chainWebpack: config => {
    config.module
      .rule('vue')
      .test(/\.vue$/)
      .use('iview-loader')
      .loader('iview-loader')
      .options({
        prefix: true,
      })
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
}
