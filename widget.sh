#!/usr/bin/env bash
# 1st arg is target directory (default current)
TARGET=${1:-.}/public/widget
echo "Target: ${TARGET}"

# build
yarn build && yarn build:loader

# make sure the target dir will exist
if [[ -e ${TARGET} ]]; then
	rm -rf ${TARGET}
fi
mkdir ${TARGET}

# deploy artifacts
mv dist ${TARGET}
mv loader/main.umd.min.js ${TARGET}/main.js
rm -rf loader
